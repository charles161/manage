import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Text, View, TouchableWithoutFeedback } from 'react-native';
import { Card, CardSection } from './common';

class ListItem extends Component {

  onRowPress() {
    Actions.employeeCreate({ employee: this.props.employee });
  }

  render() {
    const { name } = this.props.employee;
  return (
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
        <View>
          <Card>
            <CardSection>
              <Text style={{ fontSize: 20, paddingLeft: 20 }}>
                {name}
              </Text>
            </CardSection>
          </Card>
        </View>
      </TouchableWithoutFeedback>
  );
}
}

export default ListItem;
