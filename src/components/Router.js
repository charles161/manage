import React from 'react';
import { Router, Scene, Actions } from 'react-native-router-flux';
import LoginForm from './LoginForm';
import EmployeeList from './EmployeeList';
import EmployeeCreate from './EmployeeCreate';

const RouterComponent = () => {
  return (
    <Router sceneStyle={{ paddingTop: 65 }}>

      <Scene key='auth'>
        <Scene key='login' component={LoginForm} title='Login' />
      </Scene>

      <Scene key='main'>
          <Scene
          key='employeelist'
          component={EmployeeList}
          title='Employees'
          rightTitle="Add"
          onRight={() => Actions.employeeCreate()}
          initial
          />
          <Scene key='employeeCreate' component={EmployeeCreate} title='EmployeeCreate' />
      </Scene>

    </Router>
  );
};

export default RouterComponent;
