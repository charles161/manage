import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import {
  EMPLOYEE_UPDATE,
  EMPLOYEE_CREATE,
   EMPLOYEES_FETCH_SUCCESS
} from './types';

export const employeeUpdate = ({ prop, value }) => {
  return {
    type: EMPLOYEE_UPDATE,
    payload: { prop, value }
  };
};

export const employeeCreate = ({ name, phone, shift }) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
  firebase.database().ref(`/users/${currentUser.uid}/employees`)
   .push({ name, phone, shift })
   .then(() => {
     dispatch({ type: EMPLOYEE_CREATE });
     Actions.employeelist({ type: 'reset' });
   });
 };
};

export const employeesFetch = () => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
  firebase.database().ref(`/users/${currentUser.uid}/employees`)
    .on('value', snapshot => {
      dispatch({
         type: EMPLOYEES_FETCH_SUCCESS,
          payload: snapshot.val() });
    });
  };
      //any time u get a value call the function with the object snapshot
      //snapshot is an object which describes the data
      //snapshot.val() gets the data for us
      //any time we get any data the action gets dispatch ==== cool
};
