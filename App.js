

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import Router from './src/components/Router';


class App extends Component {

  componentWillMount() {
    const config = {
    apiKey: 'AIzaSyC5VK1NnoSKyo1Yhu4KIbFZM2s39-rKpNA',
    authDomain: 'manager-ab797.firebaseapp.com',
    databaseURL: 'https://manager-ab797.firebaseio.com',
    projectId: 'manager-ab797',
    storageBucket: 'manager-ab797.appspot.com',
    messagingSenderId: '40233122632'
  };
  firebase.initializeApp(config);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <Router />
      </Provider>//second argument is any initial state
    );
  }
}

export default App;
